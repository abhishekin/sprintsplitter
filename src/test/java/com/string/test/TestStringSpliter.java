package com.string.test;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.string.enums.Separator;
import com.string.util.StringSplitter;

/**
 * Utility class for string
 * 
 * @author abhi00720@gmail.com
 */
public class TestStringSpliter {

	private List<String> testList = Arrays.asList("a", "b", "c", "d");

	@Test
	public void testcase1() {
		Assert.assertTrue(testList.equals(StringSplitter.split("a,b,c,d", Separator.COMMA)));
	}

	@Test
	public void testcase2() {
		Assert.assertFalse(testList.equals(StringSplitter.split("a,b,c,d,3", Separator.COMMA)));
	}

	@Test
	public void testcase3() {
		Assert.assertFalse(testList.equals(StringSplitter.split("/,a,b,c,d", Separator.COMMA)));
	}

}
