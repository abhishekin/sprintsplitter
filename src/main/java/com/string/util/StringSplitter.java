package com.string.util;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

import com.string.enums.Separator;

/**
 * Utility class for string
 * 
 * @author abhi00720
 */
public final class StringSplitter {

	private StringSplitter() {}

	public static List<String> split(String string, Separator separator) {
		if (null == string || null == separator) {
			throw new NullPointerException();
		}
		List<String> retVal = new ArrayList<>();
		if (string.length() == 0) {
			return retVal;
		}
		AtomicInteger currentPos = new AtomicInteger(0);
		AtomicInteger nextPos = new AtomicInteger(-1);
		IntStream.range(0, string.length()).forEach(i -> {
			String substr = null;
			if (separator.getValue().equals(string.charAt(i))) {
				currentPos.set(nextPos.get() + 1);
				nextPos.set(i);
				if ((substr = string.substring(currentPos.get(), nextPos.get())).length() > 0) {
					retVal.add(substr);
				}
			} else if ((i == string.length() - 1) && (substr = string.substring(nextPos.get() + 1, i + 1)).length() > 0) {
				retVal.add(substr);
			}
		});
		return retVal;
	}
}

