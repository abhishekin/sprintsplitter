package com.string.enums;

/**
 * enum
 * 
 * @author abhi00720@gmail.com
 */
public enum Separator {

	FORWARD_SLASH('/'), BACKWARD_SLASH('\\'), SPACE(' '), COMMA(',');

	private Character value;

	Separator(Character val) {
		this.value = val;
	}

	public Character getValue() {
		return value;
	}
}
